process.env.DB_DATABASE = 'testing'

const chai = require('chai')
var should = require('chai').should()
const chaiHttp = require('chai-http')
const server = require('../../express')
const tracer = require('tracer')
const connection = require('../../src/config/connection')

chai.should()
chai.use(chaiHttp)
tracer.setLevel('error')

describe('Manage Authentication', () => {
  //let createStudentid
  describe('UC-101 Registreren', () => {
    //Before we start delete the test data
    beforeEach((done) => {
      connection.query(
        `DELETE FROM user WHERE Email = "test@test.nl"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            done()
          }
        }
      )
    })
    it('TC-101-1 Verplichte veld ontbreekt', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secret'
        })
        //Send request with missing firstname
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')

          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals(
              'AssertionError [ERR_ASSERTION]: firstname must be a string.'
            )
          //Firstname is missing so we send an error message to the body.
          done()
        })
    })
    it('TC-101-2 Invalid email', (done) => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Klaas',
          lastname: 'LastName',
          email: 'test',
          studentnr: 1234567,
          password: 'secret'
        })
        //Send request with wrong email format.
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')

          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals(
              'AssertionError [ERR_ASSERTION]: Your email input is wrong'
            )
          //Email is invalid error.
          done()
        })
    })
    it('TC-101-3 Invalide wachtwoord', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen

      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Jan',
          lastname: 'Smit',
          email: 'jsmit@server.nl',
          studentnr: '222222',
          password: 4
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')

          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals(
              'AssertionError [ERR_ASSERTION]: password must be a string.'
            )
          done()
        })
    })
    it('TC-101-4 Gebruiker bestaat al', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen

      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Jan',
          lastname: 'Smit',
          email: 'jsmit@server.nl',
          student_number: 222222,
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')

          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals('This user has already been taken.')
          done()
        })
    })

    it('TC-101-5 Gebruiker succesvol geregistreerd', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen

      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')

          const response = res.body
          response.should.have.property('token').which.is.a('string')
          response.should.have.property('username').which.is.a('string')

          done()
        })
    })
  })
})
describe('UC-102 Login', () => {
  //Before we start delete the test data
  beforeEach((done) => {
    connection.query(
      `DELETE FROM user WHERE Email = "test@test.nl"`,
      (err, rows, fields) => {
        if (err) {
          done(err)
        } else {
          done()
        }
      }
    )
  })
  it('TC-102-1 Verplicht veld ontbreekt', (done) => {
    chai
      .request(server)
      .post('/api/login')
      .send({
        password: 'secret'
      })
      //Send request with missing firstname
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')

        const { error } = res.body
        error.should.be
          .an('string')
          .that.equals(
            'AssertionError [ERR_ASSERTION]: email must be a string.'
          )
        //Firstname is missing so we send an error message to the body.
        done()
      })
  })
  it('TC-102-2 Invalide email adres', (done) => {
    chai
      .request(server)
      .post('/api/login')
      .send({
        email: 'ha123@',
        password: 'secret'
      })
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')

        const { error } = res.body
        error.should.be
          .an('string')
          .that.equals('AssertionError [ERR_ASSERTION]: Wrong email input.')
        done()
      })
  })
  it('TC-102-3 Invalide wachtwoord', (done) => {
    // server starten
    // POST versturen, onvolledige info meesturen
    // Valideren dat verwachte waarden kloppen
    chai
      .request(server)
      .post('/api/login')
      .send({
        email: 'test@test.nl',
        password: 'secresdadsa'
      })
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')

        const { error } = res.body
        error.should.be
          .an('string')
          .that.equals('User not found or password is invalid')
        done()
      })
  })
  it('TC-102-4 Gebruiker bestaat niet', (done) => {
    // server starten
    // POST versturen, onvolledige info meesturen
    // Valideren dat verwachte waarden kloppen

    chai
      .request(server)
      .post('/api/login')
      .send({
        email: 'jsmit@servehjgjhjr.nl',
        password: 'secret'
      })
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')

        const { error } = res.body
        error.should.be
          .an('string')
          .that.equals('User not found or password is invalid')
        done()
      })
  })
  it('TC-102-5 Gebruiker succesvol ingelogd', (done) => {
    // server starten
    // POST versturen, onvolledige info meesturen
    // Valideren dat verwachte waarden kloppen
    //valdiatie

    chai
      .request(server)
      .post('/api/login')
      .send({
        email: 'mark@rutte.nl',
        password: 'vvddegekste'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        done()
      })
  })
})
