const chai = require('chai')
var should = require('chai').should()
const chaiHttp = require('chai-http')
const server = require('../../express')
const tracer = require('tracer')
const connection = require('../../src/config/connection')

chai.should()
chai.use(chaiHttp)
tracer.setLevel('error')
const jwt = require('jsonwebtoken')

describe('UC-201 Maak studentenhuis', () => {
  beforeEach((done) => {
    connection.query(
      `DELETE FROM studenthome WHERE Name = "niewehuistoeveoegen"`,
      (err, rows, fields) => {
        if (err) {
          done(err)
        } else {
          done()
        }
      }
    )
  })

  it('TC-201-1 Verplicht veld ontbreekt', (done) => {
    // server starten
    // POST versturen, onvolledige info meesturen
    // Valideren dat verwachte waarden kloppen
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '3h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdsng',
          house_nr: 4,
          userId: 1,
          postal_code: '4621ZX',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          let { message, error } = res.body

          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: straatnaam is nodig')

          done()
        })
    })
  })

  it('TC-201-2  Invalide postcode ', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '3h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdsng',
          address: 'Straatnaam',
          house_nr: 4,
          userId: 1,
          postal_code: '4621',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          let { message, error } = res.body
          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')

          done()
        })
    })
  })

  it('TC-201-3  Invalide telefoonnummer', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + token)
        .send({
          homeId: '1',
          naam: 'Jan',
          straatnaam: 'Spruytstraat ',
          huisnummer: 4,
          postcode: '3081 DD',
          plaats: 'test',
          telefoonnummer: '06576'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          let { message, error } = res.body
          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: naam is nodig')

          done()
        })
    })
  })

  it('TC-201-4 Studentenhuis bestaat al', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'makkelijk',
          address: 'Adres',
          house_nr: 4,
          userId: 1,
          postal_code: '3081DD',
          telephone: 1641418357,
          city: 'dorp'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          done()
        })
    })
  })
  it('TC-201-5 Niet ingelogd', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + 'd')
        .send({
          name: 'hoasdssanewas',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '4621JE',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')
          const { error } = res.body
          error.should.be.an('string').that.equals('Not authorized')
          done()
        })
    })
  })
  it('TC-201-6 Studentenhuis  succesvol toegevoegd ', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .post('/api/studenthome')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'niewehuistoeveoegen',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '3081 DD',
          telephone: 1641418357,
          city: 'test'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')
          let { result } = res.body
          createStudentid = result.Id
          result.result.should.be.an('object')

          done()
        })
    })
  })
})
describe('UC-202 Overzichten van studentenhuizen', () => {
  it('TC-202-1 Toon nul studentenhuizen', (done) => {
    chai
      .request(server)
      .get('/api/studenthome?name=keesjan')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')
        result.result.length.should.equal(0)
        done()
      })
  })

  it('TC-202-2 Toon twee studentenhuizen', (done) => {
    chai
      .request(server)
      .get('/api/studenthome?city=dorp')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body

        result.result.length.should.equal(2)

        done()
      })
  })
  it('TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad', (done) => {
    chai

      .request(server)
      .get('/api/studenthome?city=grastad')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')
        result.result.length.should.equal(0)

        done()
      })
  })
  it('TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande naam', (done) => {
    chai
      .request(server)
      .get('/api/studenthome?name=hahastad')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')
        result.result.length.should.equal(0)

        done()
      })
  })

  it('TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad', (done) => {
    chai
      .request(server)
      .get('/api/studenthome?city=breda')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')

        done()
      })
  })

  it('TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam', (done) => {
    chai
      .request(server)
      .get('/api/studenthome?name=hoang')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')

        done()
      })
  })
})
describe('UC-203 Details van studentenhuis', () => {
  beforeEach((done) => {
    done()
  })
  it('TC-203-1 Studentenhuis-ID bestaat niet', (done) => {
    chai

      .request(server)
      .get('/api/studenthome/9922')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')
        Array.should.have.length(1)

        done()
      })
  })
  it('TC-203-2 Studentenhuis-ID bestaat', (done) => {
    chai

      .request(server)
      .get('/api/studenthome/61')
      .end((err, res) => {
        res.should.have.status(200)
        res.should.be.an('object')

        let { result } = res.body
        result.result.should.be.an('array')

        done()
      })
  })
})

describe('UC-204 Studentenhuis wijzigen', () => {
  beforeEach((done) => {
    done()
  })
  it('TC-204-1 Verplicht veld ontbreekt', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdsng',
          house_nr: 4,
          userId: 1,
          postal_code: '4621ZX',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          const { message, error } = res.body
          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: straatnaam is nodig')
          done()
        })
    })
  })
  it('TC-204-2 Invalide postcode', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdsng',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '46219Z',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          const { message, error } = res.body
          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')
          done()
        })
    })
  })
  it('TC-204-3 Invalid telefoonnummer', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdsng',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '4621 ZZ',
          telephone: 'ahaha',
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')
          const { message, error } = res.body
          message.should.be
            .an('string')
            .that.equals('An error occured while creating a student home.')
          error.should.be
            .an('string')
            .that.equals(
              'AssertionError [ERR_ASSERTION]: Phone number is incorrect'
            )
          done()
        })
    })
  })
  it('TC-204-4 Studentenhuis bestaat niet', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .put('/api/studenthome/999')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'hoasdssa',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '4621JE',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')
          const { result } = res.body
          result.should.be
            .an('string')
            .that.equals('Studenthome does not exist')
          done()
        })
    })
  })

  it('TC-204-5 Niet ingelogd', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .put('/api/studenthome/61')
        .set('authorization', 'Bearer ' + 'h')
        .send({
          name: 'testnaamupdate',
          address: 'Adres ',
          house_nr: 4,
          userId: 1,
          postal_code: '4621JE',
          telephone: 1641418357,
          city: 'breda'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')
          const { error } = res.body
          error.should.be.an('string').that.equals('Not authorized')
          done()
        })
    })
  })
  it('TC-204-6 Studentenhuis  succesvol gewijzigd', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .put('/api/studenthome/8')
        .set('authorization', 'Bearer ' + token)
        .send({
          name: 'niewuwenaam',
          address: 'Adres',
          house_nr: 4,
          userId: 1,
          postal_code: '3081DD',
          telephone: 1641418357,
          city: 'test'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')
          const { result } = res.body
          result.should.be.an('object')
          done()
        })
    })
  })
})
describe('UC-205 Studentenhuis verwijderen', () => {
  beforeEach((done) => {
    connection.query(
      `DELETE FROM studenthome WHERE Name = "niewehuistoeveoegen"`,
      (err, rows, fields) => {
        if (err) {
          done(err)
        } else {
          deleteid = rows
          done()
        }
      }
    )
  })
  it('TC-205-1 Studentenhuis bestaat niet', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .delete('/api/studenthome/9999')
        .set('authorization', 'Bearer ' + token)
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')
          const { result } = res.body
          result.should.be
            .an('string')
            .that.equals('Studenthome does not exist')
          done()
        })
    })
  })
  it('TC-205-2 Niet ingelogd', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .delete('/api/studenthome/61')
        .set('authorization', 'Bearer ' + 'h')
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')
          const { error } = res.body
          error.should.be.an('string').that.equals('Not authorized')
          done()
        })
    })
  })
  it('TC-205-3 Actor is geen eigenaar', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .delete('/api/studenthome/61')
        .set('authorization', 'Bearer ' + 'h')
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')
          const { error } = res.body
          error.should.be.an('string').that.equals('Not authorized')
          done()
        })
    })
  })

  it('TC-205-4 Studentenhuis  succesvol verwijderd', (done) => {
    jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
      chai
        .request(server)
        .delete(`/api/studenthome/82`)
        .set('authorization', 'Bearer ' + token)
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')
          const { result } = res.body
          done()
        })
    })
  })
})
