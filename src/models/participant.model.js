
class Participant {
    constructor (userId, studenthomeId, mealId,signedUpOn) {
        this.userId = userId
        this.studenthomeId = studenthomeId
        this.mealId = mealId
        this.signedUpOn = signedUpOn
    }
}
module.exports = Participant