const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')

let controller = {
  getStudentInfo(req, res, next) {
    logger.debug('Called get on api/info!')

    const info = {
      Studentnaam: 'Hoang',
      Studentnummer: '2144613',
      Beschrijving: 'Studentenhuis usecase',
      SonarQube: ''
    }
    res.status(200).json(info)
  },
  validateStudenthome(req, res, next) {
    logger.info('Validating student home...')
    try {
      const { name, address, house_nr, postal_code, city, telephone } = req.body
      assert(typeof name === 'string', 'naam is nodig')
      assert(typeof address === 'string', 'straatnaam is nodig')
      assert(typeof house_nr === 'number', 'huisnummer is nodig')
      assert(typeof postal_code === 'string', 'postcode is nodig')
      assert.match(
        postal_code,
        /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i,
        'foute postcode'
      )
      assert(typeof city === 'string', 'plaats is nodig')
      assert(typeof telephone === 'number', 'Phone number is incorrect')

      next()
    } catch (e) {
      res.status(400).json({
        message: 'An error occured while creating a student home.',
        error: e.toString()
      })
    }
  },
  createStudenthome(req, res, next) {
    const home = req.body
    database.createStudenthome(home, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        if (result.result.code === 'ER_DUP_ENTRY') {
          res.status(400).json({
            result: 'Studenthome already exists'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    })
  },

  updateStudenthomes(req, res, next) {
    logger.debug('Called post on /api/studenthome')
    updatedHome = req.body
    const homeId = req.params.homeId
    database.updateStudenthomes(req.body, homeId, (err, result) => {
      if (err) {
        logger.error('updateStudentmeal', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result.result
        })
      }
    })
  },
  getAllstudenthomes(req, res, next) {
    logger.info('Get is called on /api/studenthome?name=:name&city=:city')
    const name = req.query.name
    const city = req.query.city

    //name and city is empty
    if (name === undefined && city === undefined) {
      logger.debug('name and city is empty')
      database.getAllstudenthomes((err, result) => {
        if (err || result.result === 'Studenthome does not exist') {
          logger.error('createStudenthome', error)
          res.status(400).json({
            result: 'Studenthome does not exist'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } else if (name === undefined && city !== undefined) {
      logger.debug(' city is defined')
      database.getCity(req.query.city, (err, result) => {
        if (err || result.result === 'Studenthome does not exist') {
          logger.error('createStudenthome', error)
          res.status(400).json({
            result: 'Studenthome does not exist'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    }
    //name defined
    else if (name !== undefined && city === undefined) {
      logger.debug('name is defined ')
      database.getName(req.query.name, (err, result) => {
        if (err || result.result === 'Studenthome does not exist') {
          logger.error('createStudenthome', error)
          res.status(400).json({
            result: 'Studenthome does not exist'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    } else if (city !== undefined && name !== undefined) {
      database.getnameAndCity(name, city, (err, result) => {
        if (err || result.result === 'Studenthome does not exist') {
          logger.error('createStudenthome', error)
          res.status(400).json({
            result: 'Studenthome does not exist'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    }
  },
  deleteStudenthome(req, res, next) {
    logger.debug('Called delete on /api/studenthome')
    database.deleteStudenthome(req.params.homeId, req.body, (err, result) => {
      if (err) {
        logger.error('deleteStudenthome', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result.result
        })
      }
    })
  },
  getStudenthome(req, res, next) {
    logger.debug('Called GET on /api/studenthome')
    database.getStudenthome(req.params.homeId, (err, result) => {
      if (err) {
        logger.error('getStudenthome', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  }
}

module.exports = controller
