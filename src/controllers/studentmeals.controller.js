const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')

let controller = {
  validateStudentmeal(req, res, next) {
    logger.info('Validating student meal...')
    try {
      const {
        name,
        description,
        ingredients,
        allergies,
        createdon,
        offeredon,
        price,
        maxparticipants
      } = req.body
      assert(typeof name === 'string', 'name cannot be empty.')
      assert(typeof description === 'string', 'description cannot be empty.')
      assert(typeof ingredients === 'string', 'ingredients cannot be empty.')
      assert(typeof allergies === 'string', 'allergies cannot be empty.')
      assert(typeof createdon === 'string', 'createdon cannot be empty.')
      assert(typeof offeredon === 'string', 'offeredon cannot be empty.')
      assert(typeof price === 'number', 'price cannot be empty.')
      assert(
        typeof maxparticipants === 'number',
        'maxparticipants cannot be empty.'
      )
      next()
    } catch (err) {
      res.status(400).json({
        message: 'Meal is not valid',
        error: err.toString()
      })
    }
  },
  createStudentmeal(req, res, next) {
    const meal = req.body
    const homeid = req.params.homeId
    database.createMeal(meal, homeid, (err, result) => {
      if (err) {
        logger.error('createMeal', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: meal
        })
      }
    })
  },

  updateStudentmeal(req, res, next) {
    logger.debug('Called post on /api/studentmeals')
    const studentmeal = req.body
    database.updateMeals(
      studentmeal,
      req.params.homeId,
      req.params.mealId,
      (err, result) => {
        if (err) {
          logger.error('createStudentmeal', error)
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  },

  getAllStudentmeals(req, res, next) {
    logger.debug('Called get on /api/studentmeal')

    database.getAllstudentmeals(req.params.homeId, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },
  deleteStudentmeals(req, res, next) {
    logger.debug('Called delete on /api/studenthome')
    database.deleteMeals(
      req.params.homeId,
      req.params.mealId,
      (err, result) => {
        if (err) {
          logger.error('deleteStudentmeal', error)
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    )
  },
  getDetailOfStudentMeal(req, res, next) {
    logger.info('Get is called on /api/studenthome/.....')

    const homeId = req.params.homeId
    const mealid = req.params.mealId
    logger.info('Get is called on /api/studenthome/', homeId)

    database.getDetailOfStudentmeal(homeId, mealid, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  }
}
module.exports = controller
