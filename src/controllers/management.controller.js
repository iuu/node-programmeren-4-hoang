require('dotenv').config()
const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const jwt_decode = require('jwt-decode')
const Participant = require ('../models/participant.model')

let controller = {
  countParticipants(req, res, next) {
    let mealId = req.params.mealId
    database.verifyParticipants(mealId, (err, result) => {
      console.log(result)
      if (err) {
        logger.error('createMeal', err)
        res.status(400).json({
          result: err
        })
      } else if (result['result'] == 'There is no more space available') {
        res.status(400).json({
          result: err
        })
      } else {
        console.log('space available')
        next()
      }
    })
  },

  signUpMeal(req, res, next) {
    let { homeId, mealId } = req.params
    const userToken = jwt_decode(req.headers['authorization'])
    const user_id = userToken.id
    let participant = new Participant(user_id,homeId,mealId, "2010-01-01")
    database.signUpMeal(user_id, homeId, mealId, (err, result) => {
      if (err) {
        logger.error('signUpMeal', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: participant
        })
      }
    })
  },
  
  signOffMeal(req,res,next) {
    let { homeId, mealId } = req.params
    const userToken = jwt_decode(req.headers['authorization'])
    const user_id = userToken.id
    let participant = new Participant(user_id,homeId,mealId, "2010-01-01")
    database.signOffMeal(user_id,homeId,mealId, (err, result) => {
      if (err) {
        logger.error('signOffMeal', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: participant
        })
      }
    })
  },
  getAllParticipants(req,res,next) {
    let mealId  = req.params.mealId
    database.getAllParticipants(mealId,(err, result) => {
      if (err) {
        logger.error('getAllParticipants', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },
  getDetailOfParticipant(req,res,next) {
    let mealId = req.params.mealId
    let participantId = req.params.participantId
    database.getDetailOfParticipant(participantId,mealId, (err, result) => {
      if (err) {
        logger.error('getAllDetailofparticipant', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result
        })
      }
    })
  },
    
}
  
module.exports = controller
