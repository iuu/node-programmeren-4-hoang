const mysql = require('mysql')
const logger = require('./config').logger
const dbconfig = require('./config').dbconfig
const connection = require('./connection')
//
let database = {
  createStudenthome(studenthome, callback) {
    const {
      name,
      address,
      house_nr,
      userId,
      postal_code,
      telephone,
      city
    } = studenthome

    connection.query(
      `INSERT INTO studenthome (name, address, house_nr,userId, postal_code, telephone,city) VALUES ( '${name}', '${address}', ${house_nr}, ${userId},'${postal_code}', ${telephone},'${city}')`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else {
          callback(undefined, {
            ID: results.insertId,
            result: studenthome
          })
        }
      }
    )
  },

  updateStudenthomes(updatedHome, homeId, callback) {
    const {
      name,
      address,
      house_nr,
      userId,
      postal_code,
      city,
      telephone
    } = updatedHome

    connection.query(
      `UPDATE studenthome SET name = '${name}', address = '${address}', house_nr = ${house_nr}, userId = ${userId}, postal_code = '${postal_code}', city = '${city}', telephone = '${telephone}' WHERE ID = ${homeId}`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'Studenthome does not exist'
          })
        } else {
          callback(undefined, {
            result: updatedHome
          })
        }
      }
    )
  },
  getAllstudenthomes(callback) {
    connection.query('SELECT * FROM studenthome', function (
      err,
      results,
      fields
    ) {
      if (err) throw err
      callback(undefined, {
        result: results
      })
    })
  },

  getCity(city, callback) {
    connection.query(
      `SELECT * FROM studenthome WHERE city = "${city}"`,
      function (error, results, fields) {
        if (error) {
          console.log(err)
          res.sendStatus(500)
          return
        }
        callback(undefined, {
          result: results
        })
      }
    )
  },
  getName(name, callback) {
    connection.query(
      `SELECT * FROM studenthome WHERE name = "${name}"`,
      function (error, results, fields) {
        if (error) {
          console.log(error)
          callback(undefined, {
            result: 'Student home cannot be found'
          })
        } else {
          callback(undefined, {
            result: results
          })
        }
      }
    )
  },
  deleteStudenthome(id, body, callback) {
    connection.query(
      `DELETE IGNORE FROM studenthome WHERE id = ${id}`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'Studenthome does not exist'
          })
        } else {
          callback(undefined, {
            result: body
          })
        }
      }
    )
  },
  getStudenthome(id, callback, res) {
    connection.query(`SELECT * FROM studenthome WHERE id = ${id}`, function (
      error,
      results,
      fields
    ) {
      if (error) {
        console.log(error)
        res.status(400).json({
          message: 'verplicht veld niet ingevuld',
          error: e.toString()
        })
      } else {
        callback(undefined, {
          result: results
        })
      }
    })
  },
  getnameAndCity(name, city, callback) {
    connection.query(`SELECT * FROM studenthome WHERE Name= "${name}" AND City= "${city}"`, function(error, results, fields){
      if(error) throw error
      logger.debug('results: ', results)
      callback(undefined, {
        results: results
  
      })
    })
  },
  
  createMeal(studentmeal, homeId, callback) {
    const {
      name,
      description,
      ingredients,
      allergies,
      createdon,
      offeredon,
      price,
      userid,
      maxparticipants
    } = studentmeal

    connection.query(
      `INSERT INTO meal (name, description, ingredients, allergies, createdon,  offeredon, price, userID, studenthomeid, maxparticipants) 
    VALUES ('${name}', '${description}', '${ingredients}', '${allergies}', '${createdon}', '${offeredon}',${price}, ${userid}, ${homeId},  ${maxparticipants}) `,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else {
          callback(undefined, {
            result: studentmeal
          })
        }
      }
    )
  },

  updateMeals(updatedMeal, homeId, mealId, callback) {
    const {
      name,
      description,
      ingredients,
      allergies,
      createdon,
      offeredon,
      price,
      userid,
      maxparticipants
    } = updatedMeal

    connection.query(
      `UPDATE meal SET name = '${name}', description = '${description}', ingredients= '${ingredients}', allergies = '${allergies}', createdon = '${createdon}', offeredon =  '${offeredon}', price = ${price}, userid =  ${userid}, maxparticipants =  ${maxparticipants} WHERE studenthomeId = ${homeId} AND Id =${mealId}`,
      function (error, results, fields) {
        if (error) {
          console.log(err)
          res.sendStatus(500)
          return
        }
        callback(undefined, {
          result: updatedMeal
        })
      }
    )
  },
  deleteMeals(homeId, mealId, callback) {
    connection.query(
      `DELETE FROM meal WHERE ID = ${mealId} AND StudentHomeID = ${homeId}`,
      function (error, results, fields) {
        if (error) throw error
        callback(undefined, {
          result: results
        })
      }
    )
  },
  getAllstudentmeals(homeId, callback) {
    connection.query(
      `SELECT * FROM meal WHERE StudenthomeId = ${homeId}`,
      function (error, results, fields) {
        if (error) throw error
        console.log('results: ', results)
        callback(undefined, {
          result: results
        })
      }
    )
  },
  getDetailOfStudentmeal(homeId, mealId, callback) {
    connection.query(
      `SELECT * FROM meal WHERE studenthomeId = ${homeId} AND ID = ${mealId}`,
      function (error, results, fields) {
        if (error) throw error
        console.log('The solution is: ', results)
        callback(undefined, {
          result: results
        })
      }
    )
  },
  verifyParticipants(mealId, callback) {
    connection.query(
      `SELECT count(mealid) AS amountparticipants from participants where mealid = '${mealId}'`,
      function (error, results, fields) {
        if (error) throw error
        let amountParticipants = results[0].amountparticipants
        connection.query(
          `SELECT maxparticipants from meal where id = '${mealId}'`,
          function (error, partResult) {
            if (error) throw error
            else {
              let maxParticipants = partResult[0].maxparticipants
              console.log(amountParticipants < maxParticipants)
              if (amountParticipants < maxParticipants) {
                callback(undefined, {
                  result: 'There is space available'
                })
              } else {
                callback(undefined, {
                  result: "There is no more space available"
                })
              }
            }
          }
        )
      }
    )
  },

  signUpMeal(userId, homeId, mealId, callback) {
    connection.query(
      `INSERT INTO participants (userId, StudenthomeID, MealID, SignedUpOn) VALUES ('${userId}', '${homeId}', '${mealId}', "2010-01-01")`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else {
          callback(undefined, {
            result: results
          })
        }
      }
    )
  },
signOffMeal (userId, homeId, mealId, callback) {
  connection.query(`DELETE FROM participants where userId = '${userId}' AND mealId = '${mealId}' AND studenthomeId = '${homeId}' `,
    function (error, results, fields) {
    if (error) {
      callback(undefined, {
        result: error
      })
    } else {
      callback(undefined, {
        result: results
      })
    }
  }
)
},

getAllParticipants(mealId,callback) {
  connection.query(`SELECT * FROM participants where mealId = '${mealId}' `,
    function (error, results, fields) {
    if (error) {
      callback(undefined, {
        result: error
      })
    } else {
      callback(undefined, {
        result: results
      })
    }
  }
)
},
getDetailOfParticipant(userId,mealId,callback) {
  connection.query(`SELECT user.id, user.first_name, user.last_name,
   user.email, user.student_number
    FROM user JOIN participants ON participants.userid = user.id 
    JOIN meal ON meal.id = participants.mealid WHERE user.id = '${userId}' 
    AND meal.id = '${mealId}' `, function (error, results, fields) {
      if (error) {
        callback(undefined, {
          result: error
        })
      } else {
        callback(undefined, {
          result: results
        })
      }
    }
  )
}
}
module.exports = database
