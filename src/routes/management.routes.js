const express = require('express')
const router = express.Router()
const usercontroller = require('../controllers/studentmeals.controller')
const authcontroller = require('../controllers/authentication.controller')
const managementcontroller = require('../controllers/management.controller')

// studenthome routers

//UC-401 Aanmelden voor maaltijd
router.post(
  '/studenthome/:homeId/meal/:mealId/signup',
  authcontroller.validateToken,
  managementcontroller.countParticipants,
  managementcontroller.signUpMeal

)

//UC-402 Afmelden voor maaltijd
router.put(
  '/studenthome/:homeId/meal/:mealId/signoff',
  authcontroller.validateToken,
  managementcontroller.signOffMeal
)

//UC-403 Lijst van deelnemers opvragen
router.get('/meal/:mealId/participants',
authcontroller.validateToken,
managementcontroller.getAllParticipants)

// //UC-404 Details van deelnemer opvragen
router.get(
  '/meal/:mealId/participants/:participantId',
  authcontroller.validateToken,
  managementcontroller.getDetailOfParticipant
)

module.exports = router
