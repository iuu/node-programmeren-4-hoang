const express = require('express')
const router = express.Router()
const studentmealcontroller = require('../controllers/studentmeals.controller')
const authcontroller = require('../controllers/authentication.controller')

//UC-301 Maaltijd aanmaken
router.post(
  '/studenthome/:homeId/meal',
  authcontroller.validateToken,
  studentmealcontroller.validateStudentmeal,
  studentmealcontroller.createStudentmeal
)
//UC-302 Maaltijd wijzigen
router.put(
  '/studenthome/:homeId/meal/:mealId',
  authcontroller.validateToken,
  studentmealcontroller.validateStudentmeal,
  studentmealcontroller.updateStudentmeal
)
//UC-303 Lijst van maaltijden opvragen
router.get(
  '/studenthome/:homeId/meal',
  studentmealcontroller.getAllStudentmeals
)
//UC-304 Details van een maaltijd opvragen
router.get(
  '/studenthome/:homeId/meal/:mealId',
  studentmealcontroller.getDetailOfStudentMeal
)
//UC-305 Maaltijd
router.delete(
  '/studenthome/:homeId/meal/:mealId',
  authcontroller.validateToken,
  studentmealcontroller.deleteStudentmeals
)
module.exports = router
