const express = require('express')
const router = express.Router()
const studentshomecontroller = require('../controllers/studenthomes.controller')
const authcontroller = require('../controllers/authentication.controller')

// studenthome routers

//UC-103, get student info
router.get('/info', studentshomecontroller.getStudentInfo)

//UC-201, validate, create studenthome
router.post(
  '/studenthome',
  authcontroller.validateToken,
  studentshomecontroller.validateStudenthome,
  studentshomecontroller.createStudenthome
)

//UC-202, gives a list of all studenthouses
router.get('/studenthome', studentshomecontroller.getAllstudenthomes)

//UC-203, get studenthome with ID param
router.get('/studenthome/:homeId', studentshomecontroller.getStudenthome)

//UC-204, update studenthome with new info
router.put(
  '/studenthome/:homeId',
  authcontroller.validateToken,
  studentshomecontroller.validateStudenthome,
  studentshomecontroller.updateStudenthomes
)

//UC-205, delete a studenthome
router.delete(
  '/studenthome/:homeId',
  authcontroller.validateToken,
  studentshomecontroller.deleteStudenthome
)

module.exports = router
