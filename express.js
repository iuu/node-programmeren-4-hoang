const express = require('express')
const bodyParser = require('body-parser')
const studenthomeroutes = require('./src/routes/studenthomes.routes')
const studentmealroutes = require('./src/routes/studentmeals.routes')
const authenticationroutes = require('./src/routes/authentication.routes')
const management = require('./src/routes/management.routes')
const config = require('./src/config/config')
const logger = config.logger
const app = express()
require('dotenv').config()
app.use(bodyParser.json())

const port = process.env.PORT || 3000

//app.all/app.get('*')

app.all('*', (req, res, next) => {
  const method = req.method
  logger.debug('Method:', method)
  next()
})

app.use('/api', studenthomeroutes)
app.use('/api', studentmealroutes)
app.use('/api', authenticationroutes)
app.use('/api', management)

app.all('*', (req, res, next) => {
  res.status(404).json({
    error: 'Endpoint does not exist!'
  })
})
app.listen(port, function () {
  logger.info(`Server running at port ${port}`)
})

module.exports = app
